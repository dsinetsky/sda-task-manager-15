package ru.t1.dsinetsky.tm.enumerated;

import ru.t1.dsinetsky.tm.exception.system.InvalidStatusException;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static String getStatusList() {
        StringBuilder statusName = new StringBuilder();
        for (Status status : values())
            statusName.append(" - ").append(status.name()).append("\n");
        return statusName.toString();
    }

    public static Status toStatus(String value) throws InvalidStatusException {
        if (value == null || value.isEmpty()) throw new InvalidStatusException();
        for (Status status : values())
            if (value.equals(status.name())) return status;
        throw new InvalidStatusException();
    }

    public String getDisplayName() {
        return displayName;
    }

}
