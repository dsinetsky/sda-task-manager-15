package ru.t1.dsinetsky.tm.api.controller;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public interface IProjectTaskController {

    void bindTaskToProjectById() throws GeneralException;

    void unbindTaskToProjectById() throws GeneralException;

}
