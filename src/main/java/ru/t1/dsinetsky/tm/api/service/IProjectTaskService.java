package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectTaskService {

    void bindProjectById(String projectId, String taskId) throws GeneralException;

    void unbindProjectById(String projectId, String taskId) throws GeneralException;

    Project removeProjectById(String projectId) throws GeneralException;

    Project removeProjectByIndex(int index) throws GeneralException;

}
