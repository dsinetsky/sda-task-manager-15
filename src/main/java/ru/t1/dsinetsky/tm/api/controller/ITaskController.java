package ru.t1.dsinetsky.tm.api.controller;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public interface ITaskController {

    void showTasks() throws GeneralException;

    void createTask() throws GeneralException;

    void clearTasks();

    void showTaskById() throws GeneralException;

    void showTaskByIndex() throws GeneralException;

    void showTasksByProjectId() throws GeneralException;

    void updateTaskById() throws GeneralException;

    void updateTaskByIndex() throws GeneralException;

    void removeTaskById() throws GeneralException;

    void removeTaskByIndex() throws GeneralException;

    void createTestTasks() throws GeneralException;

    void startTaskById() throws GeneralException;

    void startTaskByIndex() throws GeneralException;

    void completeTaskById() throws GeneralException;

    void completeTaskByIndex() throws GeneralException;

    void changeStatusById() throws GeneralException;

    void changeStatusByIndex() throws GeneralException;

}
