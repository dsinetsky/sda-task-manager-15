package ru.t1.dsinetsky.tm.exception.field;

public class NameIsEmptyException extends GeneralFieldException {

    public NameIsEmptyException() {
        super("Name cannot be empty!");
    }

}
