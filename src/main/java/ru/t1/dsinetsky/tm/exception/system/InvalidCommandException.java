package ru.t1.dsinetsky.tm.exception.system;

public class InvalidCommandException extends GeneralSystemException {

    public InvalidCommandException() {
        super("Invalid command! Type \"help\" for list of commands!");
    }

    public InvalidCommandException(String message) {
        super("Command " + message + " is not supported! Type \"help\" for list of commands!");
    }

}
