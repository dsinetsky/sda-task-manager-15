package ru.t1.dsinetsky.tm.exception.entity;

public class TaskNotFoundException extends GeneralEntityException {

    public TaskNotFoundException() {
        super("Task not found!");
    }
}
