package ru.t1.dsinetsky.tm.exception.system;

public class InvalidStatusException extends GeneralSystemException {

    public InvalidStatusException() {
        super("Invalid status!");
    }

}
