package ru.t1.dsinetsky.tm.exception.field;

public class ProjectIdIsEmptyException extends GeneralFieldException {

    public ProjectIdIsEmptyException() {
        super("Project Id cannot be empty!");
    }

}
