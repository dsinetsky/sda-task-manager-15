package ru.t1.dsinetsky.tm.exception.field;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public class GeneralFieldException extends GeneralException {

    public GeneralFieldException() {
    }

    public GeneralFieldException(String message) {
        super(message);
    }

    public GeneralFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralFieldException(Throwable cause) {
        super(cause);
    }

    public GeneralFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
