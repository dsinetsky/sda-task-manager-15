package ru.t1.dsinetsky.tm.exception.field;

public class DescIsEmptyException extends GeneralFieldException {

    public DescIsEmptyException() {
        super("Description cannot be empty!");
    }

}
