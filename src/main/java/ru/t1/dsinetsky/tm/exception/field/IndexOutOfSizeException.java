package ru.t1.dsinetsky.tm.exception.field;

public class IndexOutOfSizeException extends GeneralFieldException {

    public IndexOutOfSizeException(int size) {
        super("Index must be between 1 and " + size + "!");
    }

}
