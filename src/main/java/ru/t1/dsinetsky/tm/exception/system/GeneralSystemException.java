package ru.t1.dsinetsky.tm.exception.system;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public class GeneralSystemException extends GeneralException {

    public GeneralSystemException() {
    }

    public GeneralSystemException(String message) {
        super(message);
    }

    public GeneralSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralSystemException(Throwable cause) {
        super(cause);
    }

    public GeneralSystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
