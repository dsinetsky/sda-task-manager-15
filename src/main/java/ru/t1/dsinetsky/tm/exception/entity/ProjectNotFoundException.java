package ru.t1.dsinetsky.tm.exception.entity;

public class ProjectNotFoundException extends GeneralEntityException {

    public ProjectNotFoundException() {
        super("Project not found!");
    }

}
