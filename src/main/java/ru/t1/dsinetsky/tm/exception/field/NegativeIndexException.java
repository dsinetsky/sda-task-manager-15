package ru.t1.dsinetsky.tm.exception.field;

public class NegativeIndexException extends GeneralFieldException {

    public NegativeIndexException() {
        super("Index must be greater than zero!");
    }

}
