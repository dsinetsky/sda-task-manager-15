package ru.t1.dsinetsky.tm.exception.field;

public class TaskIdIsEmptyException extends GeneralFieldException {

    public TaskIdIsEmptyException() {
        super("Task Id cannot be empty!");
    }

}
