package ru.t1.dsinetsky.tm.exception.entity;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public class GeneralEntityException extends GeneralException {

    public GeneralEntityException() {
    }

    public GeneralEntityException(String message) {
        super(message);
    }

    public GeneralEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralEntityException(Throwable cause) {
        super(cause);
    }

    public GeneralEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
