package ru.t1.dsinetsky.tm.model;

import ru.t1.dsinetsky.tm.api.model.IWBS;
import ru.t1.dsinetsky.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Project implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String desc = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }


    //@Override
    //public String toString() {
    //    return new StringBuilder(name).append(" : ").append(desc).toString();
    //}
    //Show statuses for testing project status sort
    @Override
    public String toString() {
        return new StringBuilder(name).append(" : ").append(desc).append(". Status: ").append(Status.toName(status)).toString();
    }
}
